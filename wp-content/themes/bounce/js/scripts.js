$(document).ready(function(){
    $('.mapOverlay i').click(function(){
       $(this).parent().parent().fadeOut('fast'); 
    });
    
    $('.fpProjectsRow .project .overlay').hover(function(){
        $(this).parent().parent().removeClass('invert-image');
        $(this).css('background', 'transparent');
        $(this).find('.pName').css('display', 'none');
        $(this).find('.plus').css('display', 'block');
    }, function(){
        $(this).parent().parent().addClass('invert-image');
        $(this).css('background', 'rgba(0, 0, 0, 0.85)');
        $(this).find('.plus').css('display', 'none');
        $(this).find('.pName').css('display', 'block');
    });

    $(function() {
        $('#myNavbar a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });

    $('.headRow .arrowDown i').click(function () {
        $('html, body').animate({
                scrollTop: $('#about').offset().top
            }, 1000);
            return false;
    })
    
    if ($(window).width() > 767){
        $('#blog-slider').lightSlider({
            gallery: false,
            item: 1,
            thumbItem: 0,
            slideMargin: 0,
            speed: 500,
            auto: true,
            loop: true,
            vertical: true,
            verticalHeight: 320,
            enableDrag: false,
            controls: false,
            pause: 5000
        });
    } else {
        $('#blog-slider').lightSlider({
            gallery: false,
            item: 1,
            thumbItem: 0,
            slideMargin: 0,
            speed: 500,
            auto: true,
            loop: true,
            vertical: false,
            // verticalHeight: 320,
            enableDrag: false,
            controls: false,
            pause: 5000
        });
    }
    
    $("#team-slider").lightSlider({
        gallery: false,
        item: 1,
        thumbItem: 0,
        slideMargin: 0,
        speed: 500,
        auto: true,
        loop: true,
        enableDrag: false,
        controls: false,
        pause: 5000
    });

    $('#contactForm').validate({
        rules:{
            name:{
                required: true,
                minlength: 3
            },
            email:{
                email: true,
                required: true
            },
            subject:{
                required: true,
                minlength: 3
            },
            message:{
                required: true,
                minlength: 3
            }
        },
        messages:{
            name:{
                required: '* Required field',
                minlength: 'Min length - 3 sym'
            },
            email:{
                email: 'Enter correct email',
                required: '* Required field'
            },
            subject:{
                required: '* Required field',
                minlength: 'Min length - 3 sym'
            },
            message:{
                required: '* Required field',
                minlength: 'Min length - 3 sym'
            }
        },
        submitHandler: function() {
            var name = $('#contactForm #name').val();
            var email = $('#contactForm #email').val();
            var subject = $('#contactForm #subject').val();
            var message = $('#contactForm #message').val();
            $.ajax({
                url: "/wp-admin/admin-ajax.php",
                type:     "POST",
                data : "action=contact&name=" + name + "&email=" + email + "&subject=" + subject + "&message=" + message,
                success: function(response) {
                    result = jQuery.parseJSON(response);
                    if (result.error === ""){
                        $('#thanksMessage').modal('show');
                        $('#contactForm input[type="text"], #contactForm textarea').val('');
                    }
                },
                error: function(response) {

                }
            });
        }
    });
    
})