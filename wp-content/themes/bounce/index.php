<?php
/**
 * Главная страница (index.php)
 * @package WordPress
 * @subpackage bounce-theme
 * Template Name: Главная страница
 */
get_header(); ?> 

<?php include(locate_template( "parts/about.php" )); ?>
    
<?php include(locate_template( "parts/featured_projects.php" )); ?> 
    
<?php include(locate_template( "parts/services.php" )); ?> 
    
<?php include(locate_template( "parts/portfolio.php" )); ?> 

<?php include(locate_template( "parts/team.php" )); ?> 
    
<?php include(locate_template( "parts/blog.php" )); ?>
    
<?php include(locate_template( "parts/contacts.php" )); ?>

<?php get_footer(); ?>