<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage bounce-theme
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- <header>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<nav class="navbar navbar-default">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topnav" aria-expanded="false">
								<span class="sr-only">Меню</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="collapse navbar-collapse" id="topnav">
							<?php $args = array( 
								'theme_location' => 'top',
								'container'=> false,
						  		'menu_id' => 'top-nav-ul',
						  		'items_wrap' => '<ul id="%1$s" class="nav navbar-nav %2$s">%3$s</ul>',
								'menu_class' => 'top-menu',
						  		'walker' => new bootstrap_menu(true)		  		
					  			);
								wp_nav_menu($args);
							?>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</header> -->

	<section id="head">
	    <div class="container-fluid">
	        <div class="row headRow">
	            <div class="container">
	                <div class="row">
	                    <nav class="navbar navbar-default">
	                      <div class="container-fluid">
	                        <div class="navbar-header">
	                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
	                            <span class="icon-bar"></span>
	                            <span class="icon-bar"></span>
	                            <span class="icon-bar"></span> 
	                          </button>
	                          <a class="navbar-brand brand" href="#">Bouncy</a>
	                        </div>
	                        <div class="collapse navbar-collapse" id="myNavbar">
	                          <ul class="nav navbar-nav navbar-right">
	                            <li class="active"><a href="#head">Hello</a></li>
	                            <li><a href="#about">About</a></li>
	                            <li><a href="#services">Services</a></li> 
	                            <li><a href="#portfolio">Portfolio</a></li> 
	                            <li><a href="#team">Team</a></li> 
	                            <li><a href="#blog">Blog</a></li> 
	                            <li><a href="#contact">Contact</a></li> 
	                          </ul>
	                        </div>
	                      </div>
	                    </nav>
	                    <div class="col-xs-12 info center">
	                        <p class="title">We Are Code Cafe</p>
	                        <p>
	                            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis <br/> praesentium voluptatum
	                        </p>
	                    </div>
	                    <div class="col-xs-12 center arrowDown">
	                        <i class="fa fa-angle-down"></i>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>


<div id="thanksMessage" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<i class="fa fa-close"></i>
					</button>
				</div>
				<div class="modal-body">
					<p class="modal-title center">Thank You!</p>
					<hr class="modal-line">
					<p class="modal-desc center">Your appeal will be considered <br> and we'll contact You asap!</p>
				</div>
			</div>
		</div>
	</div>