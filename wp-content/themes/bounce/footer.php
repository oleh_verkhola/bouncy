<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage bounce-theme
 */
?>
	<!-- <footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php $args = array(
						'theme_location' => 'bottom',
						'container'=> false,
						'menu_class' => 'nav nav-pills bottom-menu',
				  		'menu_id' => 'bottom-nav',
				  		'fallback_cb' => false
				  	);
					wp_nav_menu($args);
					?>
				</div>
			</div>
		</div>
	</footer> -->

	<footer>
	    <div class="container">
	        <div class="row">
	            <div class="col-xs-12 center">
	                <p class="brand name">Bouncy</p>
	                <p>Copyright <?php echo date('Y'); ?>. All rights Reserved</p>
	                <div class="social">
	                    <a href="#" target="_blank">
	                        <i class="fa fa-facebook"></i>
	                    </a>
	                    <a href="#" target="_blank">
	                        <i class="fa fa-twitter"></i>
	                    </a>
	                    <a href="#" target="_blank">
	                        <i class="fa fa-linkedin"></i>
	                    </a>
	                    <a href="#" target="_blank">
	                        <i class="fa fa-instagram"></i>
	                    </a>
	                    <a href="#" target="_blank">
	                        <i class="fa fa-vimeo"></i>
	                    </a>
	                </div>
	            </div>
	        </div>
	    </div>
	</footer>
<?php wp_footer(); ?>
</body>
</html>
