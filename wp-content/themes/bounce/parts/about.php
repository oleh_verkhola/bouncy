<?php 

$args = array(
    'post_type' => 'about',
    'orderby' => 'id',
    'order' => 'ASC'
    );
$abouts = get_posts($args);

?>

<section id="about">
    <div class="container-fluid">
        <div class="row aboutRow">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 center">
                        <p class="section-title">Details about Bouncy</p>
                        <p class="section-description">
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui <br/> blanditiis praesentium
                        </p>
                        <ul class="nav nav-tabs about-tabs">
                            <?php $i = 0; foreach ($abouts as $key => $value) { ?>
                            <li class="<?php echo ($i == 0) ? 'active' : ''; ?>">
                                <a data-toggle="tab" href="#<?php echo $value->ID; ?>">
                                    <?php echo get_the_post_thumbnail( $value->ID, 'full' ); ?>
                                </a>
                            </li>
                            <?php $i++; } ?>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-offset-1 col-lg-10">
                        <div class="tab-content about-tab-content">
                          <?php $i = 0; foreach ($abouts as $key => $value) { ?>
                              <div id="<?php echo $value->ID; ?>" class="tab-pane fade <?php echo ($i == 0) ? 'in active' : ''; ?>">
                                <div class="row">
                                    <div class="col-xs-12 first-column">
                                        <p class="Title"><?php echo $value->post_title; ?></p>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 first-column">
                                        <?php echo get_field('first_description', $value->ID); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 second-column">
                                        <?php echo get_field('second_description', $value->ID); ?>
                                    </div>
                                </div>
                              </div>
                          <?php $i++; } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>