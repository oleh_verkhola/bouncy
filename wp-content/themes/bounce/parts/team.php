<?php 

$args = array(
    'post_type' => 'team',
    'orderby' => 'id',
    'order' => 'ASC'
    );
$team = get_posts($args);

?>

<section id="team">
    <div class="container-fluid">
        <div class="row aboutRow teamRow">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 center">
                        <p class="section-title">The team</p>
                        <p class="section-description">
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui <br/> blanditiis praesentium
                        </p>
                    </div>
                </div>
                <div class="row teamSliderRow">
                    <div class="col-lg-offset-1 col-lg-10">
                        <ul id="team-slider" class="team-slider">
                            <?php foreach ($team as $key => $value) { ?>
                                <li class="item">
                                    <?php echo get_the_post_thumbnail( $value->ID, 'full' ); ?>
                                    <div class="info">
                                        <div class="worker-info">
                                            <p class="name"><?php echo $value->post_title; ?></p>
                                            <p class="position"><?php echo get_field('position', $value->ID); ?></p>
                                        </div>
                                        <div class="desc">
                                            <?php echo get_field('short_description', $value->ID); ?>
                                        </div>
                                        <div class="skills">
                                            <div class="skill">
                                                <span>branding</span> 
                                                <span class="percent"><?php echo get_field('branding', $value->ID); ?>%</span>
                                                <hr style="
                            -webkit-border-image: -webkit-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 100%); 
                            -moz-border-image: -moz-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 100%); 
                            -o-border-image: -o-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 100%); 
                            border-image: linear-gradient(to right, #19bd9a 0%, #19bd9a <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 <?php echo get_field('branding', $value->ID); ?>%, #e1e4e9 100%);">
                                            </div>
                                            <div class="skill">
                                                <span>web design</span> 
                                                <span class="percent"><?php echo get_field('web_design', $value->ID); ?>%</span>
                                                <hr style="
                            -webkit-border-image: -webkit-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 100%); 
                            -moz-border-image: -moz-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 100%); 
                            -o-border-image: -o-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 100%); 
                            border-image: linear-gradient(to right, #19bd9a 0%, #19bd9a <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 <?php echo get_field('web_design', $value->ID); ?>%, #e1e4e9 100%);">
                                            </div>
                                            <div class="skill">
                                                <span>user interface</span> 
                                                <span class="percent"><?php echo get_field('user_interface', $value->ID); ?>%</span>
                                                <hr style="
                            -webkit-border-image: -webkit-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 100%); 
                            -moz-border-image: -moz-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 100%); 
                            -o-border-image: -o-linear-gradient(right, #19bd9a 0%, #19bd9a <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 100%); 
                            border-image: linear-gradient(to right, #19bd9a 0%, #19bd9a <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 <?php echo get_field('user_interface', $value->ID); ?>%, #e1e4e9 100%);">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>