<?php 

$args = array(
    'post_type' => 'blog',
    'orderby' => 'id',
    'order' => 'DESC'
    );
$posts = get_posts($args);
// print_r($posts);
?>


<section id="blog">
    <div class="container-fluid">
        <div class="row aboutRow blogRow">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 ">
                        <p class="section-title">Latest News</p>
                        <p class="section-description">
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui <br/> blanditiis praesentium
                        </p>
                    </div>
                </div>
                <div class="row blogSliderRow">
                    <div class="col-lg-offset-1 col-lg-11">
                        <ul id="blog-slider" class="blog-slider">
                            <?php foreach ($posts as $key => $value) { ?>
                                <li class="item">
                                    <p class="title"><?php echo $value->post_title; ?></p>
                                    <div class="author">by <span class="author_name"><?php echo get_the_author_meta('display_name', $value->post_author); ?></span></div>
                                    <div class="comments">
                                        6950 Likes - <?php echo $value->comment_count; ?> Comments - 703 shares
                                    </div>
                                    <div class="text">
                                        <?php echo $value->post_content; ?>
                                    </div>
                                    <a href="#" class="read_more">+ Read More</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>