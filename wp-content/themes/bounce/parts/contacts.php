 <section id="contact">
    <div class="container">
        <div class="row contactRow">
            <div class="col-lg-offset-1 col-lg-10">
                <form id="contactForm" method="post" action="">
                    <div class="form-group">
                        <input type="text" name="name" id="name" value="" placeholder="Your name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="email" id="email" value="" placeholder="Email address">
                    </div>
                    <div class="form-group">
                        <input type="text" name="subject" id="subject" value="" placeholder="Subject">
                    </div>
                    <textarea name="message" id="message"></textarea>
                    <input type="submit" value="Submit now">
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row mapRow">
            <div id="map"></div>
            <div class="col-xs-12 mapOverlay center">
                <div class="inside">
                    <span>Find us on the map</span> <i class="fa fa-angle-down"></i>
                </div>
            </div>
            <script>
              function initMap() {
                var uluru = {lat: 51.522409, lng: 0.031520};
                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 18,
                  center: uluru
                });
                var marker = new google.maps.Marker({
                  position: uluru,
                  map: map
                });
              }
            </script>
            <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCnIu_XGdHBXa2ehH82_aMkJ2kda6ofQt8&callback=initMap">
            </script>
        </div>
    </div>
</section>