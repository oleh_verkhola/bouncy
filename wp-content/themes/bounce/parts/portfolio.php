<section id="portfolio">
    <div class="container-fluid">
        <div class="row aboutRow portfolioRow">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 center">
                        <p class="section-title">The Portfolio</p>
                        <p class="section-description">
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui <br/> blanditiis praesentium
                        </p>
                        <ul class="nav nav-tabs portfolio-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#all">All Works</a>
                                <hr>    
                            </li>
                            <li>
                                <a data-toggle="tab" href="#print">Print</a>
                                <hr>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#identity">Identity</a>
                                <hr>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#branding">Branding</a>
                                <hr>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#web">Web</a>
                                <hr>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#html">Html</a>
                                <hr>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#wordpress">Wordpress</a>
                                <hr>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-offset-1 col-lg-10">
                        <div class="tab-content portfolio-tab-content">
                          <div id="all" class="tab-pane fade in active">
                              <div class="row">
                                <div class="col-md-3">
                                  <div class="row well">
                                      Column A. Block 1<br />
                                  </div>
                                  <div class="row well">
                                      Column A. Block 2<br /> <br /> <br /> <br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column A. Block 3<br /> <br /> <br />
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="row well">
                                      Column B. Block 1<br /> <br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column B. Block 2<br /> <br /> <br /><br /><br /><br />
                                  </div>
                                  <div class="row well">
                                      Column B. Block 3<br /> <br />
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="row well">
                                      Column C. Block 1<br />
                                  </div>
                                  <div class="row well">
                                      Column C. Block 2<br /> <br /> <br /> <br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column C. Block 3<br /> <br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column C. Block 4<br /> <br /> <br />
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="row well">
                                      Column D. Block 1<br /> <br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column D. Block 2<br /> <br /> <br /> <br /> <br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column D. Block 3<br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column D. Block 3<br /> <br />
                                  </div>
                                  <div class="row well">
                                      Column D. Block 3<br /> <br />
                                  </div>
                              </div>
                                  <!-- <div class="col-lg-3">
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                  </div>

                                  <div class="col-lg-3">
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                  </div>

                                  <div class="col-lg-3">
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                  </div>

                                  <div class="col-lg-3">
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                      <div class="item" style="border: 1px solid #000;">
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                          <h4>Lorem ipsum</h4>
                                      </div>
                                  </div> -->
                              </div>
                          </div>
                          <div id="print" class="tab-pane fade">
                              Print
                          </div>
                          <div id="identity" class="tab-pane fade">
                              Identity
                          </div>
                          <div id="branding" class="tab-pane fade">
                              Branding
                          </div>
                          <div id="web" class="tab-pane fade">
                              Web
                          </div>
                          <div id="html" class="tab-pane fade">
                              Html
                          </div>
                          <div id="wordpress" class="tab-pane fade">
                              Wordpress
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>