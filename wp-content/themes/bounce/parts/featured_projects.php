<?php 

$args = array(
    'post_type' => 'f_projects',
    'orderby' => 'id',
    'order' => 'ASC',
    'numberposts' => 3
    );
$projects = get_posts($args);

?>

<section id="featured-projects">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row fpTitleRow">
                    <div class="col-xs-12 center">
                        <p class="section-title">Featured projects</p>
                        <p class="section-description">
                            At vero eos et accusamus et iusto odio dignissimos ducimus qui <br/> blanditiis praesentium
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row fpProjectsRow">
            <?php $i = 0; foreach ($projects as $key => $value) { ?>
                <div class="col-xs-12 <?php echo ($i == 0) ? 'col-sm-6' : 'col-sm-3'; ?> project invert-image" style="
                background: url(<?php echo get_the_post_thumbnail_url( $value->ID, "full" ); ?>); background-size: cover;">
                    <a href="#">
                        <div class="overlay center">
                            <div class="inside">
                                <span class="pName"><?php echo $value->post_title; ?></span>
                                <img src="/wp-content/themes/bounce/img/fp-plus.png" alt=" " class="plus">
                            </div>
                        </div>                
                    </a>
                </div>   
            <?php $i++; } ?>
        </div>
        <div class="row fpBottomRow">
            <div class="container">
                <div class="row">
                    <div class="col-lg-offset-1 col-lg-10">
                        <span>At vero eos et <b>accusamus</b> et iusto odio dignissimos ducimus qui blanditiis </span>
                        <a href="#">Submit Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>